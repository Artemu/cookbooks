    execute "update apt" do
      command "sudo apt-get update"
      action :run
    end

    execute "install php" do
      ignore_failure true
      command "apt-get install -y php5-fpm php5-cli php5-cgi php5-memcached php-apc php5-curl php5-mcrypt"
      action :run
    end